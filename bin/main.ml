open Aocaml

let usage_msg = "aocaml -d <day>"
let day = ref 0
let example = ref false

let anon_fun _ = ()

let speclist =
    [
        ("-d", Arg.Set_int day, "The day number");
        ("-e", Arg.Set example, "If the day is an example")
    ]

let openf day is_ex =
    let res = File.get_fpath day is_ex in
    match res with
        | Error msg -> Error msg
        | Ok fpath -> Ok(File.read_lines(fpath))

let () =
    Arg.parse speclist anon_fun usage_msg;
    let res = openf !day !example in
    match res with
        | Ok lines -> List.iter (print_endline) lines
        | Error msg -> print_endline (Prints.format_err msg)
