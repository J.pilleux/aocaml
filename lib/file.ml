let num_to_str n =
    if n < 10 then Ok("0" ^ (Int.to_string n))
    else if n <= 31 then Ok (Int.to_string n)
    else Error "Day should be between 1 and 31"

let get_ext is_example =
    if is_example then ".example"
    else ".data"

let get_fpath n is_example =
    let day_num = num_to_str n in
    match day_num with
        | Error msg -> Error msg
        | Ok value -> Ok (Sys.getcwd() ^ "/resources/day_" ^ value ^ (get_ext is_example))

let read_lines fpath =
    let chan = open_in fpath in
    let rec read_lines_helper acc =
        try
            let line = input_line chan in
            read_lines_helper (line :: acc)
        with
            | End_of_file -> close_in chan; List.rev acc
    in
    read_lines_helper []
