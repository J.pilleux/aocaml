(** Returns the file path of a data file *)
val get_fpath : int -> bool -> (string, string) result

val read_lines : string -> string list
