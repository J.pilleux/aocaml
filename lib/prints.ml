let red s = "\027[31m" ^ s ^ "\027[0m"

let format_err msg = red ("Error: ") ^ msg
