(** Format a message to an error message *)
val format_err : string -> string
