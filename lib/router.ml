let toto _ = Error "Err"

let run day is_example exercise =
    let res = File.get_fpath day is_example in
    match res with
        | Error msg -> Error msg
        | Ok fpath ->
            let lines = File.read_lines(fpath) in
            exercise lines

let route day is_example =
    match day with
        | 1 -> run day is_example toto
        | _ -> Error "Day not implemented"
